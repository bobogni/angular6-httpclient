import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categorie-add',
  templateUrl: './categorie-add.component.html',
  styleUrls: ['./categorie-add.component.css']
})
export class CategorieAddComponent implements OnInit {

  @Input() categorieData = { name:'', description:''};

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  addCategorie() {
    this.rest.addCategorie(this.categorieData).subscribe((result) => {
      console.log(result.data);
      this.router.navigate(['/edit-categorie/'+result.data['id']]);
    }, (err) => {
      
      console.log(err);
    });
  }

}
