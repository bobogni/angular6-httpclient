import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { ArticleComponent } from './article/article.component';
import { ArticleAddComponent } from './article-add/article-add.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';
import { CategorieAddComponent } from './categorie-add/categorie-add.component';
import { CategorieDetailComponent } from './categorie-detail/categorie-detail.component';
import { CategorieEditComponent } from './categorie-edit/categorie-edit.component';


@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ArticleAddComponent,
    ArticleDetailComponent,
    ArticleEditComponent,
    CategorieAddComponent,
    CategorieDetailComponent,
    CategorieEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
