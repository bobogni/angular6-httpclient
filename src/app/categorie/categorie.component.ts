import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {

  categories:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.categories = [];
    this.rest.getCategories().subscribe((data: {}) => {
      console.log(data);
      this.categories = data;
    });
  }

  add() {
    this.router.navigate(['/categorie-add']);
  }

  delete(id) {
    this.rest.deleteCategorie(id)
      .subscribe(res => {
          this.getCategories();
        }, (err) => {
          console.log(err);
        }
      );
  }


}
