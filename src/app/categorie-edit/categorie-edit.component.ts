import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-categorie-edit',
  templateUrl: './categorie-edit.component.html',
  styleUrls: ['./categorie-edit.component.css']
})
export class CategorieEditComponent implements OnInit {

  @Input() categorieData:any = { name: '', description: '' };

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getCategorie(this.route.snapshot.params['id']).subscribe((data) => {
      //console.log(this.route.snapshot.params['id']);
      console.log(data);
      this.categorieData = data.data;
    });
  }

  updateCategorie(id) {
    this.rest.updateCategorie(this.route.snapshot.params['id'], this.categorieData).subscribe((result) => {
      this.router.navigate(['/edit-categorie/'+result.data['id']]);
    }, (err) => {
      console.log(err);
    });
  }

}
