import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  articles:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getArticles();
  }

  getArticles() {
    this.articles = [];
    this.rest.getArticles().subscribe((data: {}) => {
      console.log(data);
      this.articles = data;
    });
  }

  add() {
    this.router.navigate(['/article-add']);
  }

  delete(id) {
    this.rest.deleteArticle(id)
      .subscribe(res => {
          this.getArticles();
        }, (err) => {
          console.log(err);
        }
      );
  }

}
