import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {

  @Input() articleData:any = { title: '', content: '', categorie_id: '' };

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getArticle(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.articleData = data;
    });
  }

  updateArticle() {
    this.rest.updateArticle(this.route.snapshot.params['id'], this.articleData).subscribe((result) => {
      this.router.navigate(['/article-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
  }


}
