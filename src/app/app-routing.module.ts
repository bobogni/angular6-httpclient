import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategorieAddComponent } from './categorie-add/categorie-add.component';
import { CategorieDetailComponent } from './categorie-detail/categorie-detail.component';
import { CategorieEditComponent } from './categorie-edit/categorie-edit.component';
import { ArticleAddComponent } from './article-add/article-add.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/register-categorie', pathMatch: 'full' },
  { path: 'register-categorie', component: CategorieAddComponent },
  { path: 'view-categorie', component: CategorieDetailComponent },
  { path: 'edit-categorie/:id', component: CategorieEditComponent },

  { path: 'register-article', component: ArticleAddComponent },
  { path: 'view-article', component: ArticleDetailComponent },
  { path: 'edit-article/:id', component: ArticleEditComponent }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
