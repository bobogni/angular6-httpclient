import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost:8000/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

   // ****************** Methodes crud de categorie *******************//

  getCategories(): Observable<any> {
    return this.http.get(endpoint + 'categories').pipe(
      map(this.extractData));
  }
  
  getCategorie(id): Observable<any> {
    return this.http.get(endpoint + 'categories/' + id).pipe(
      map(this.extractData));
  }
  
  addCategorie (categorie): Observable<any> {
    console.log(categorie);
    return this.http.post<any>(endpoint + 'categories', JSON.stringify(categorie), httpOptions).pipe(
      tap((product) => console.log(`added categorie w/ id=${categorie.id}`)),
      catchError(this.handleError<any>('addCategorie'))
    );
  }
  
  updateCategorie (id, categorie): Observable<any> {
    return this.http.put(endpoint + 'categories/' + id, JSON.stringify(categorie), httpOptions).pipe(
      tap(_ => console.log(`updated categorie id=${id}`)),
      catchError(this.handleError<any>('updateCategorie'))
    );
  }
  
  deleteCategorie (id): Observable<any> {
    return this.http.delete<any>(endpoint + 'categories/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted categorie id=${id}`)),
      catchError(this.handleError<any>('deleteCategorie'))
    );
  }

  // ****************** Methodes crud de article *******************//

  getArticles(): Observable<any> {
    return this.http.get(endpoint + 'articles').pipe(
      map(this.extractData));
  }
  
  getArticle(id): Observable<any> {
    return this.http.get(endpoint + 'articles/' + id).pipe(
      map(this.extractData));
  }
  
  addArticle (article): Observable<any> {
    console.log(article);
    return this.http.post<any>(endpoint + 'articles', JSON.stringify(article), httpOptions).pipe(
      tap((product) => console.log(`added article w/ id=${article.id}`)),
      catchError(this.handleError<any>('addArticle'))
    );
  }
  
  updateArticle (id, article): Observable<any> {
    return this.http.put(endpoint + 'articles/' + id, JSON.stringify(article), httpOptions).pipe(
      tap(_ => console.log(`updated article id=${id}`)),
      catchError(this.handleError<any>('updateArticle'))
    );
  }
  
  deleteArticle (id): Observable<any> {
    return this.http.delete<any>(endpoint + 'articles/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted article id=${id}`)),
      catchError(this.handleError<any>('deleteArticle'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
