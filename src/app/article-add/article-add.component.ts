import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {
 
  articleData = { title:'', content: '', categorie_id: ''};
  categorieData = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getCategories().subscribe((data) => {
      //console.log(this.route.snapshot.params['id']);
      console.log("MonTexte",data);
      this.categorieData = data.data;
    });

  }

  addArticle() {
    this.rest.addCategorie(this.articleData).subscribe((result) => {
      this.router.navigate(['/edit-article/'+result.data['id']]);
    }, (err) => {
      console.log(err);
    });
  }

}
